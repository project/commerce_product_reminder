CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Similar modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module allows visitors to subscribe on a commerce product to be notify by
mail when a new product variation is published (or an existing variation
unpublished is updated and published).

The pimary usage is, for example, if your product is an event or a training
course, and you want your visitors can subscribe to this product and be
notified when a new session of the event or formation is available.

Another usage could be to subscribe on a product if its variations are out of
stock. Anyway stocks are managed, you can condition the Reminder form in a Twig
template to be render only if the product has not anymore variations published
(so you have to configure variation to be unpublished if they come out of
tock). You can look at another solution such Commerce Stock Notifications if
you are looking for a specific solution to handle notification per mail about
stock.

This module provide an extra field which expose a form on the product page to
allow anonymous visitors to let their email to be notified once a new variation
is published. You should configure this Extra Field on the relevant view mode
(the full view mode generally) for each product bundle on which a reminder has
been enabled.

You can configure the form exposed from the Reminder settings configuration
form (text and submit label are configurable)

Also you can configure the email sent for each subscriber (mail from, subject
and body) from the same configuration page.

Any visitor receiving a reminder email will have a special link included in
the email footer to allow this anonymous user to unsubscribe to existing
reminders related to his email. There is no need to authenticate any users. The
module is designed to work with anonymous users.

REQUIREMENTS
------------

Commerce Product Reminder requires the following modules.

 - Commerce
 - Commerce Product
 - Token
 - Text (Core module)

OpenSSL extension is required on your server.

SIMILAR MODULES
-------------------

You can look at Commerce Stock Notifications if you are looking for a specific
solution to handle notification per mail about stock.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.

CONFIGURATION
-------------

All the configuration happens under /admin/commerce/config/products/reminder.

General settings:
 * Configure the commerce product bundles on which you want enable subscription.
 * Configure to use cron for sending notifications mails.
 * You can stop sending mails with the enabled settings.

Reminder form
 * Configure a text displayed above the subscription form
 * Configure the submit button label
 * Configure the message displayed after a successfully subscription

Mail settings

 * Configure the mail from. Leave empty to use the mail set on a default store,
   or hte site mail if no store available
 * Configure the mail subject
 * Configure the body email

Token are supported for commerce product and commerce product variation
entities.

Permissions

 * Configure the module's permissions.
 * The "subscribe reminder" permission is required to be able to subscribe a
   reminder on a product.

Product view mode

 * Configure the extra field Reminder Form on the relevant product view modes.


TROUBLESHOOTING
---------------


FAQ
---



MAINTAINERS
-----------

Current maintainer:
 * flocondetoile
   - https://drupal.org/u/flocondetoile
   - https://www.flocondetoile.fr

