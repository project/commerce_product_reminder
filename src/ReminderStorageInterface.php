<?php

namespace Drupal\commerce_product_reminder;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * The subscription storage.
 */
interface ReminderStorageInterface extends ContentEntityStorageInterface {

  /**
   * Loads all subscriptions for a source entity.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The product.
   * @param bool $active_only
   *   Get only the reminders enabled.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface[]
   *   The reminders related to the product.
   */
  public function loadMultipleByProduct(ProductInterface $product, bool $active_only = TRUE);

  /**
   * Loads all reminders for a given owner.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   * @param bool $active_only
   *   Get only the reminders enabled.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface[]
   *   The reminders related to the user.
   */
  public function loadMultipleByUser(AccountInterface $account, bool $active_only = TRUE);

  /**
   * Loads all reminders given a mail.
   *
   * @param string $mail
   *   The mail.
   * @param bool $active_only
   *   Get only the reminders enabled.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface[]
   *   The reminders related to the mail.
   */
  public function loadMultipleByMail($mail, bool $active_only = TRUE);

  /**
   * Loads a reminder given a product and a mail.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The product.
   * @param string $mail
   *   The mail.
   * @param bool $active_only
   *   Gets only reminder enabled.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface[]
   *   The reminders related to the product and the mail.
   */
  public function loadMultipleByProductAndMail(ProductInterface $product, string $mail, bool $active_only = TRUE);

  /**
   * Gets the reminder given a product and a mail.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The product entity.
   * @param $mail
   *   The mail.
   * @param bool $active_only
   *   Get only the reminders enabled.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface|null
   */
  public function getReminderFor(ProductInterface $product, $mail, bool $active_only = TRUE);

}
