<?php

namespace Drupal\commerce_product_reminder;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product_reminder\Entity\ReminderInterface;

/**
 * Interface HelperServiceInterface.
 */
interface HelperServiceInterface {


  /**
   * Get the bundles of an entity type.
   *
   * @param bool $with_label
   * @param string $entity_type
   *
   * @return mixed
   */
  public function getEntityTypeBundles($with_label = TRUE, $entity_type = 'commerce_product');

  /**
   * Get (and set if NULL) the private key.
   */
  public function getPrivateKey();

  /**
   * Get (and set if NULL) the private iv.
   */
  public function getPrivateIv();

  /**
   * Base64 encode a string.
   *
   * @param $data
   *
   * @return string
   */
  public function base64urlEncode($data): string;

  /**
   * Base64 decode a string.
   *
   * @param $data
   *
   * @return bool|string
   */
  public function base64urlDecode($data);

  /**
   * Encrypt a string.
   *
   * @param $string
   *   The string to encrypt.
   *
   * @return string
   *   The string encrypted.
   */
  public function encrypt($string): string;

  /**
   * Decrypt a string.
   *
   * @param $string
   *   The string encrypted.
   *
   * @return string
   *   The string decrypted.
   */
  public function decrypt($string): string;

  /**
   * Generate the hash key used for the unsubscribe link.
   *
   * @param $mail
   * @param $entity_id
   *
   * @return string
   */
  function generateHash($mail, $entity_id): string;

  /**
   * Load a reminder given a product and an email.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   * @param $mail
   * @param bool $active_only
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface|mixed|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadReminderByProductAndMail(ProductInterface $product, $mail, bool $active_only = TRUE);

  /**
   * Load all reminders, active or not for a product.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface[]
   */
  public function loadRemindersByProduct(ProductInterface $product): array;

  /**
   * Load a Reminder object using the hash.
   *
   * @param $hash
   * @param bool $active_only
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface|mixed|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadReminderByHash($hash, bool $active_only = TRUE);

  /**
   * Replace token in a string given a product and a product variation.
   *
   * @param $string
   * @param \Drupal\commerce_product\Entity\ProductInterface|NULL $product
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface|NULL $product_variation
   *
   * @return string
   */
  public function replaceToken($string, ProductInterface $product = NULL, ProductVariationInterface $product_variation = NULL): string;

  /**
   * Get the Reminders entities related to a product.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation
   *
   * @return array|\Drupal\commerce_product_reminder\Entity\ReminderInterface[]
   */
  public function getRemindersFromVariation(ProductVariationInterface $product_variation): array;

  /**
   * Use cron or not to send emails.
   *
   * @return bool
   */
  public function useCron(): bool;

  /**
   * Log is enabled in global settings.
   *
   * @return bool
   */
  public function shouldLog(): bool;

  /**
   * Log a reminder deletion.
   *
   * @param \Drupal\commerce_product_reminder\Entity\ReminderInterface $reminder
   *
   */
  public function logDeletion(ReminderInterface $reminder);

  /**
   * Log a reminder deletion error.
   *
   * @param \Drupal\commerce_product_reminder\Entity\ReminderInterface $reminder
   *
   */
  public function logDeletionError(ReminderInterface $reminder);

  /**
   * Sending mail is enabled ?
   *
   * @return bool
   */
  public function isEnabled(): bool;

  /**
   * Confirmation mail is enabled ?
   *
   * @return bool
   */
  public function confirmationIsEnabled(): bool;

  /**
   * Get the mail subject.
   *
   * @return string
   */
  public function getSubject(): string;

  /**
   * Get the mail body.
   *
   * @return array
   *   The body array with value and format.
   */
  public function getBody(): array;

  /**
   * Get the mail from email.
   *
   * @return string
   */
  public function getEmailFrom(): string;

  /**
   * Get the confirmation mail subject.
   *
   * @return string
   */
  public function getConfirmationSubject(): string;

  /**
   * Get the confirmation mail body.
   *
   * @return array
   *   The body array with value and format.
   */
  public function getConfirmationBody(): array;

  /**
   * Get the confirmation mail from email.
   *
   * @return string
   */
  public function getConfirmationEmailFrom(): string;

  /**
   * Mask a mail address.
   *
   * For example, name@example.org will be masked as n*****@e*****.org.
   *
   * @param $mail
   *   A valid mail address to mask.
   *
   * @return string
   *   The masked mail address.
   */
  function maskEmail($mail): string;

  /**
   * Send a reminder mail.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation
   *   The commerce product variation entity.
   * @param \Drupal\commerce_product_reminder\Entity\ReminderInterface $reminder
   *   The reminder entity.
   *
   * @return bool
   */
  public function sendMail(ProductVariationInterface $product_variation, ReminderInterface $reminder): bool;

  /**
   * Send a confirmation reminder mail.
   *
   * @param \Drupal\commerce_product_reminder\Entity\ReminderInterface $reminder
   *   The reminder entity.
   *
   * @return bool
   */
  public function sendConfirmationMail(ReminderInterface $reminder): bool;

}
