<?php

namespace Drupal\commerce_product_reminder\Entity;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Defines the Reminder entity.
 *
 * @ingroup commerce_product_reminder
 *
 * @ContentEntityType(
 *   id = "commerce_product_reminder",
 *   label = @Translation("Reminder"),
 *   label_singular = @Translation("reminder"),
 *   label_plural = @Translation("reminders"),
 *   label_count = @PluralTranslation(
 *     singular = "@count reminder",
 *     plural = "@count reminders",
 *   ),
 *   handlers = {
 *     "event" = "Drupal\commerce_product_reminder\Event\ReminderEvent",
 *     "storage" = "Drupal\commerce_product_reminder\ReminderStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\commerce_product_reminder\ReminderListBuilder",
 *     "views_data" = "Drupal\commerce_product_reminder\Entity\ReminderViewsData",
 *     "form" = {
 *       "default" = "Drupal\commerce_product_reminder\Form\ReminderForm",
 *       "add" = "Drupal\commerce_product_reminder\Form\ReminderForm",
 *       "edit" = "Drupal\commerce_product_reminder\Form\ReminderForm",
 *       "delete" = "Drupal\commerce_product_reminder\Form\ReminderDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "access" = "Drupal\commerce_product_reminder\ReminderAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_product_reminder\ReminderHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "commerce_product_reminder",
 *   admin_permission = "administer reminder entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "mail",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/reminder/{commerce_product_reminder}",
 *     "add-form" = "/admin/content/reminder/add",
 *     "edit-form" = "/admin/content/reminder/{commerce_product_reminder}/edit",
 *     "delete-form" = "/admin/content/reminder/{commerce_product_reminder}/delete",
 *     "delete-multiple-form" = "/admin/content/reminder/delete",
 *     "collection" = "/admin/content/reminder",
 *   },
 *   field_ui_base_route = "commerce_product_reminder.settings"
 * )
 */
class Reminder extends ContentEntityBase implements ReminderInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * Whether currently copying field values from corresponding User.
   *
   * @var bool
   */
  protected static $syncing;

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUser() {
    $uid = $this->getUserId();
    if ($uid && ($user = User::load($uid))) {
      return $user;
    }
    elseif ($mail = $this->getMail()) {
      return user_load_by_mail($mail) ?: NULL;
    }
    else {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getUserId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUser(AccountInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public function getMail() {
    return $this->get('mail')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMail($mail) {
    $this->set('mail', $mail);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode() {
    return $this->get('langcode')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLangcode($langcode) {
    $this->set('langcode', $langcode);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProduct() {
    return $this->get('product_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getProductId() {
    return $this->get('product_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setProduct(ProductInterface $product) {
    $this->set('product_id', $product->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setProductId($product_id) {
    $this->set('product_id', $product_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters() {
    return $this->get('parameters')->first()->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setParameters(array $parameters) {
    $this->set('parameters', $parameters);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function fillFromUser(UserInterface $user) {
    if (static::$syncing) {
      return $this;
    }

    static::$syncing = TRUE;
    $this->set('uid', $user->id());
    static::$syncing = FALSE;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postCreate(EntityStorageInterface $storage) {
    parent::postCreate($storage);
    // Fill from a User account with matching uid or email.
    if ($user = $this->getUser()) {
      $this->fillFromUser($user);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setDescription(t('The corresponding user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Email'))
      ->setDescription(t("The reminder's email address."))
      ->setSetting('default_value', '')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', array(
        'type' => 'email_default',
        'settings' => array(),
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['product_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Product'))
      ->setDescription(t('The product subscribed.'))
      ->setSetting('target_type', 'commerce_product')
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('A boolean indicating whether the Reminder is enabled.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 50,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['parameters'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Parameters'))
      ->setDescription(t('A serialized array of additional parameters.'));

    return $fields;
  }

}
