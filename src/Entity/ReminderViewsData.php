<?php

namespace Drupal\commerce_product_reminder\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Reminder entities.
 */
class ReminderViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    return $data;
  }

}
