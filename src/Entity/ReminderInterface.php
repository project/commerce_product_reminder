<?php

namespace Drupal\commerce_product_reminder\Entity;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface for defining Reminder entities.
 *
 * @ingroup commerce_product_reminder
 */
interface ReminderInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Gets the Reminder mail.
   *
   * @return string
   *   Name of the Reminder.
   */
  public function getMail();

  /**
   * Sets the Reminder mail.
   *
   * @param string $mail
   *   The Reminder mail.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface
   *   The called Reminder entity.
   */
  public function setMail($mail);

  /**
   * Gets the Reminder creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Reminder.
   */
  public function getCreatedTime();

  /**
   * Sets the Reminder creation timestamp.
   *
   * @param int $timestamp
   *   The Reminder creation timestamp.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface
   *   The called Reminder entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Reminder status indicator.
   *
   * @return bool
   *   TRUE if the Reminder is enabled.
   */
  public function isEnabled();

  /**
   * Gets the status of a Reminder.
   *
   * @return bool
   *   The status boolean.
   */
  public function getStatus();

  /**
   * Sets the status of a Reminder.
   *
   * @param bool $status
   *   TRUE to set this Reminder to enabled, FALSE to set it to disabled.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface
   *   The called Reminder entity.
   */
  public function setStatus($status);

  /**
   * Returns the lang code.
   *
   * @return string
   *   The subscribers lang code.
   */
  public function getLangcode();

  /**
   * Sets the lang code.
   *
   * @param string $langcode
   *   The subscribers lang code.
   */
  public function setLangcode($langcode);

  /**
   * Returns corresponding user ID.
   *
   * @return int
   *   The corresponding user ID.
   */
  public function getUserId();

  /**
   * Returns corresponding User object, if any.
   *
   * @return \Drupal\user\UserInterface|null
   *   The corresponding User object, or NULL if the reminder is not synced
   *   with a user.
   */
  public function getUser();

  /**
   * Sets the user id.
   *
   * @param int $uid
   *   The user uid.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface
   *   The called Reminder entity.
   */
  public function setUserId($uid);

  /**
   * Sets the user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user object.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface
   *   The called Reminder entity.
   */
  public function setUser(AccountInterface $account);

  /**
   * Fill values from a user account.
   *
   * @param \Drupal\user\UserInterface $user
   *   The account to fill from.
   *
   * @return $this
   */
  public function fillFromUser(UserInterface $user);

  /**
   * Gets the product subscribed.
   *
   * @return \Drupal\commerce_product\Entity\ProductInterface
   *   The product.
   */
  public function getProduct();

  /**
   * Gets the product ID
   *
   * @return int
   *   The product ID.
   */
  public function getProductId();

  /**
   * Sets the product.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $product
   *   The product entity.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface
   *   The called Reminder entity.
   */
  public function setProduct(ProductInterface $product);

  /**
   * Sets the product ID.
   *
   * @param int
   *   The product ID;
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface
   *   The called Reminder entity.
   */
  public function setProductId($product_id);

  /**
   * Gets additional parameters.
   *
   * @return array
   *   The parameters.
   */
  public function getParameters();

  /**
   * Sets additional parameters.
   *
   * @param array $parameters
   *   The parameters.
   *
   * @return $this
   */
  public function setParameters(array $parameters);

}
