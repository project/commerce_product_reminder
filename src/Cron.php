<?php

namespace Drupal\commerce_product_reminder;

use Drupal\commerce_product_reminder\Interval;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;

/**
 * Default cron implementation.
 *
 * Queues unconfirmed reminder for expiration (deletion).
 *
 * @see \Drupal\commerce_product_reminder\Plugin\QueueWorker\ReminderDeletionWorker
 */
class Cron implements CronInterface {

  /**
   * The reminder storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $reminderStorage;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The commerce_cart_expiration queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Constructs a new Cron object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory,QueueFactory $queue_factory) {
    $this->reminderStorage = $entity_type_manager->getStorage('commerce_product_reminder');
    $this->configFactory = $config_factory;
    $this->queue = $queue_factory->get('commerce_product_reminder_deletion_worker');
  }

  /**
   * {@inheritdoc}
   */
  public function run() {
    if (!$this->getConfig()->get("confirmation.purge")) {
      return;
    }
    $purge_expiration = $this->getConfig()->get("confirmation.expiration");
    if (empty($purge_expiration)) {
     return;
    }

    $interval = new Interval($purge_expiration['number'], $purge_expiration['unit']);
    $all_ids = $this->getReminderIds($interval);
    foreach (array_chunk($all_ids, 50) as $ids) {
      $this->queue->createItem($ids);
    }

  }

  /**
   * Gets the settings config object.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The configuration.
   */
  protected function getConfig(): ImmutableConfig {
    return $this->configFactory->get('commerce_product_reminder.settings');
  }

  /**
   * Gets the reminder unconfirmed IDs.
   *
   * @param \Drupal\commerce_product_reminder\Interval $interval
   *   The expiration interval.
   *
   * @return array
   *   The reminder IDs.
   */
  protected function getReminderIds(Interval $interval) {
    $current_date = new DrupalDateTime('now');
    $expiration_date = $interval->subtract($current_date);
    $query = $this->reminderStorage->getQuery()
      ->range(0, 250)
      ->condition('status', FALSE)
      ->condition('created', $expiration_date->getTimestamp(), '<=')
      ->accessCheck(FALSE)
      ->addTag('commerce_product_reminder_purge');
    $ids = $query->execute();
    return $ids;
  }

}
