<?php

namespace Drupal\commerce_product_reminder\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_product_reminder\HelperServiceInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Drupal\commerce_product_reminder\HelperServiceInterface definition.
   *
   * @var \Drupal\commerce_product_reminder\HelperServiceInterface
   */
  protected $helper;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Component\Utility\EmailValidatorInterface definition.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;


  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\commerce_product_reminder\HelperServiceInterface $commerce_product_reminder_helper
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(ConfigFactoryInterface $config_factory, HelperServiceInterface $commerce_product_reminder_helper, EntityTypeManagerInterface $entity_type_manager, EmailValidatorInterface $email_validator) {
    parent::__construct($config_factory);
    $this->helper = $commerce_product_reminder_helper;
    $this->entityTypeManager = $entity_type_manager;
    $this->emailValidator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('commerce_product_reminder.helper'),
      $container->get('entity_type.manager'),
      $container->get('email.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_product_reminder.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_product_reminder_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_product_reminder.settings');

    $form['secret'] = [
      '#type' => 'details',
      '#title' => $this->t('Secret keys'),
      '#open' => (bool) (empty($config->get('private_key')) && empty($config->get('private_iv'))),
    ];

    $private_key = $config->get('private_key') ?: $this->t('Private key will be generated on the submit form.');
    $form['secret']['private_key'] = [
      '#type' => 'item',
      '#markup' => $private_key,
      '#title' => $this->t('Private key'),
    ];

    $private_iv = $config->get('private_iv') ?: $this->t('Private IV will be generated on the submit form.');
    $form['secret']['private_iv'] = [
      '#type' => 'item',
      '#markup' => $private_iv,
      '#title' => $this->t('Private IV'),
    ];

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];

    $form['general']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Commerce Product Reminder'),
      '#description' => $this->t('Uncheck this option will stop sending mails to subscribers for new variations.'),
      '#default_value' => $config->get('general.enabled'),
    ];

    $form['general']['use_cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use cron to send mail'),
      '#description' => $this->t('Check this option to use cron for sending mails. Otherwise mails will be sent directly after adding a new variation.'),
      '#default_value' => $config->get('general.use_cron'),
    ];

    $form['general']['log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log the subscriptions created and deleted'),
      '#description' => $this->t('Check this option to log every reminder created by visitors, or deleted.'),
      '#default_value' => $config->get('general.log'),
    ];

    $bundles = $this->helper->getEntityTypeBundles();
    $form['general']['bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enable Reminder on these Commerce Product bundles'),
      '#options' => $bundles,
      '#description' => $this->t('Leave empty to enable on all the bundles available.'),
      '#default_value' => $config->get('general.bundles') ?: [],
    ];

    $form['form'] = [
      '#type' => 'details',
      '#title' => $this->t('Form settings'),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];

    $form['form']['submit_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message displayed above the email field'),
      '#default_value' => $config->get('form.submit_message.value'),
      '#format' => $config->get('form.submit_message.format') ?: filter_fallback_format(),
    ];

    $form['form']['submit_label'] = [
      '#type' => 'textfield',
      '#default_value' => $config->get('form.submit_label'),
      '#title' => $this->t('Text used for the submit button'),
    ];

    $form['form']['status_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('The Message displayed after a visitor has subscribed successfully to a product.'),
      '#default_value' => $config->get('form.status_message.value'),
      '#format' => $config->get('form.status_message.format') ?: filter_fallback_format(),
    ];

    $form['form']['manage_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message displayed on the manage subscriptions page.'),
      '#default_value' => $config->get('form.manage_message.value'),
      '#format' => $config->get('form.manage_message.format') ?: filter_fallback_format(),
    ];


    $form['mail'] = [
      '#type' => 'details',
      '#title' => $this->t('Mail settings'),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];

    $form['mail']['from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email From'),
      '#description' => $this->t('The Email used as the sender for emails sent. Leave empty to use the default Email store.'),
      '#default_value' => $config->get('mail.from'),
    ];

    $form['mail']['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Mail body'),
      '#default_value' => $config->get('mail.body.value'),
      '#format' => $config->get('mail.body.format') ?: filter_fallback_format(),
    ];

    $form['mail']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail subject'),
      '#default_value' => $config->get('mail.subject'),
    ];

    $form['mail']['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['commerce_product' => 'commerce_product', 'commerce_product_variation' => 'commerce_product_variation'],
    ];

    $form['confirmation'] = [
      '#type' => 'details',
      '#title' => $this->t('Confirmation settings'),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];
    $form['confirmation']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable confirmation'),
      '#description' => $this->t('Check this option to ask visitor to confirm their subscription. If enabled, reminder created will be inactive until the visitor confirm the subscription.'),
      '#default_value' => $config->get('confirmation.enabled'),
    ];
    $form['confirmation']['purge'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Purge unconfirmed reminder'),
      '#description' => $this->t('Check this option to purge automatically unconfirmed reminder. If enabled, reminder not confirmed older than the interval below should be deleted.'),
      '#default_value' => $config->get('confirmation.purge'),
    ];
    $expiration = $config->get('confirmation.expiration');
    $form['confirmation']['expiration'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['interval'],
      ],
      '#states' => [
        'visible' => [
          ':input[name="confirmation[purge]"]' => ['checked' => TRUE],
        ],
      ],
      '#open' => TRUE,
    ];
    $form['confirmation']['expiration']['number'] = [
      '#type' => 'number',
      '#title' => t('Interval'),
      '#default_value' => !empty($expiration['number']) ? $expiration['number'] : 30,
      '#required' => TRUE,
      '#min' => 1,
    ];
    $form['confirmation']['expiration']['unit'] = [
      '#type' => 'select',
      '#title' => t('Unit'),
      '#title_display' => 'invisible',
      '#default_value' => !empty($expiration['unit']) ? $expiration['unit'] : 'day',
      '#options' => [
        'minute' => t('Minute'),
        'hour' => t('Hour'),
        'day' => t('Day'),
        'month' => t('Month'),
      ],
      '#required' => TRUE,
    ];
    $form['confirmation']['mail'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mail settings'),
      '#tree' => TRUE,
    ];
    $form['confirmation']['mail']['from'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Confirmation Email From'),
      '#description' => $this->t('The Email used as the sender for confirmation emails sent. Leave empty to use the default Email store.'),
      '#default_value' => $config->get('confirmation.mail.from'),
    ];

    $form['confirmation']['mail']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Confirmation Email subject'),
      '#default_value' => $config->get('confirmation.mail.subject'),
    ];

    $form['confirmation']['mail']['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Confirmation Email body'),
      '#default_value' => $config->get('confirmation.mail.body.value'),
      '#format' => $config->get('confirmation.mail.body.format') ?: filter_fallback_format(),
    ];

    $form['confirmation']['mail']['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['commerce_product' => 'commerce_product',],
    ];
    $form['#attached']['library'][] = 'commerce_product_reminder/admin';
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!empty($values['mail']['from'])) {
      if (!$this->emailValidator->isValid($values['mail']['from'])) {
        $form_state->setError($form['mail']['from'], $this->t('The email provided is not valid.'));
      }
    }
    if (!empty($values['confirmation']['mail']['from'])) {
      if (!$this->emailValidator->isValid($values['confirmation']['mail']['from'])) {
        $form_state->setError($form['confirmation']['mail']['from'], $this->t('The email provided is not valid.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('commerce_product_reminder.settings')
      ->set('general', $values['general'])
      ->set('form', $values['form'])
      ->set('mail', $values['mail'])
      ->set('confirmation', $values['confirmation'])
      ->save();

    // Generate the private key and iv if not set. The getter methods will set
    // them if they not exists yet.
    $private_key = $this->helper->getPrivateKey();
    $private_iv = $this->helper->getPrivateIv();

    parent::submitForm($form, $form_state);
  }

}
