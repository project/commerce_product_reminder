<?php

namespace Drupal\commerce_product_reminder\Form;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product_reminder\Entity\ReminderInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_product_reminder\HelperServiceInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Flood\FloodInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ReminderForm.
 */
class ReminderManageForm extends FormBase {

  /**
   * The window in second a visitor can attempt to reach this form.
   */
  const IP_WINDOW = 3600;

  /**
   * The number of times a visitor can try to access this form with wrong hash.
   */
  const IP_LIMIT = 5;

  /**
   * The flood event name.
   */
  const FLOOD_EVENT = 'commerce_product_reminder.manage_reminder';

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\commerce_product_reminder\HelperServiceInterface definition.
   *
   * @var \Drupal\commerce_product_reminder\HelperServiceInterface
   */
  protected $helper;

  /**
   * Drupal\Component\Utility\EmailValidatorInterface definition.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Drupal\Core\Flood\FloodInterface definition.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Drupal\commerce\CommerceContentEntityStorage definition.
   *
   * @var \Drupal\commerce\CommerceContentEntityStorage
   */
  protected $productStorage;

  /**
   * Drupal\commerce_product_reminder\ReminderStorageInterface definition.
   *
   * @var \Drupal\commerce_product_reminder\ReminderStorageInterface
   */
  protected $reminderStorage;

  /**
   * ReminderSubscriptionForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\commerce_product_reminder\HelperServiceInterface $helper
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   * @param \Drupal\Core\Flood\FloodInterface $flood
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, HelperServiceInterface $helper, EmailValidatorInterface $email_validator, FloodInterface $flood, RendererInterface $renderer) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->helper = $helper;
    $this->emailValidator = $email_validator;
    $this->flood = $flood;
    $this->renderer = $renderer;
    $this->productStorage = $entity_type_manager->getStorage('commerce_product');
    $this->reminderStorage = $entity_type_manager->getStorage('commerce_product_reminder');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('commerce_product_reminder.helper'),
      $container->get('email.validator'),
      $container->get('flood'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reminder_manage_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $hash = NULL) {
    $config = $this->configFactory->get('commerce_product_reminder.settings');
    if (!$this->flood->isAllowed(self::FLOOD_EVENT, self::IP_LIMIT, self::IP_WINDOW)) {
      throw new AccessDeniedHttpException();
    }
    $default_reminder = NULL;
    $mail = $form_state->get('mail_to_confirm');
    if (empty($mail)) {
      $default_reminder = $this->helper->loadReminderByHash($hash);
      if (!$default_reminder instanceof ReminderInterface) {
        $this->flood->register(self::FLOOD_EVENT, self::IP_WINDOW);
        throw new NotFoundHttpException();
      }
      $mail = $default_reminder->getMail();
      $form_state->set('mail_to_confirm', $mail);
    }

    $mail_masked = $this->helper->maskEmail($mail);
    $options = [];
    $reminders = $this->reminderStorage->loadMultipleByMail($mail);
    foreach ($reminders as $reminder) {
      if (!$reminder->getProduct()) {
        continue;
      }
      $options[$reminder->id()] = $reminder->getProduct()->label();
    }

    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Manage your reminder notifications for @mail_masked', ['@mail_masked' => $mail_masked]),
      '#wrapper_attributes' => ['class' => ['mange-reminders__message']],
      '#weight' => 0,
    ];

    $form['reminders'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Your reminders'),
      '#tree' => TRUE,
    ];

    if (empty($options)) {
      $form['reminders']['empty'] = [
        '#type' => 'markup',
        '#markup' => $this->t("You don't have any more reminder notifications for @mail_masked", ['@mail_masked' => $mail_masked]),
        '#wrapper_attributes' => ['class' => ['mange-reminders__message']],
        '#weight' => 0,
      ];
    }
    else {
      $form['reminders']['ids'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Select the product for which you want to delete your reminder.'),
        '#options' => $options,
        '#default_value' => $default_reminder ? [$default_reminder->id()] : [],
      ];
    }

    $form['mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Confirm Email'),
      '#description' => $this->t('Please confirm your mail to be allowed to deleted your reminders'),
      '#maxlength' => 255,
      '#attributes' => ['class'=> ['js-reminder-mail']],
      '#default_value' => '',
      '#required' => TRUE,
      '#size' => 64,
      '#weight' => 10,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['#weight'] = 100;
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete the reminders selected'),
      '#button_type' => 'primary',
    ];

    $form['#attached']['library'][] = 'commerce_product_reminder/reminder_form';
    $form['#attributes']['class'][] = 'js-mail-control';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (empty($values['mail'])) {
      $form_state->setError($form['mail'], $this->t('The confirm email is mandatory.'));
    }
    if (!$this->emailValidator->isValid($values['mail'])) {
      $form_state->setError($form['mail'], $this->t('Looks like the confirm email provided is not valid.'));
    }

    if (empty($values['reminders']['ids'])) {
      $form_state->setError($form, $this->t('None reminders have been selected for deletion.'));
    }
    else {
      $reminder_ids = array_filter($values['reminders']['ids']);
      if (empty($reminder_ids)) {
        $form_state->setError($form, $this->t('None reminders have been selected for deletion.'));
      }
    }

    $mail_confirmed = $values['mail'];
    $mail_to_confirm = $form_state->get('mail_to_confirm');
    if ($mail_confirmed !== $mail_to_confirm) {
      $form_state->setError($form['mail'], $this->t("The confirm email doesn't match the email for which you have reminders notifications."));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (empty($values['reminders']['ids'])) {
      $form_state->setRebuild();
      return;
    }
    $reminder_ids = array_filter($values['reminders']['ids']);
    if (empty($reminder_ids)) {
      $form_state->setRebuild();
      return;
    }
    $reminders = $this->reminderStorage->loadMultiple($reminder_ids);
    foreach ($reminders as $reminder) {
      if (!$reminder instanceof ReminderInterface) {
        continue;
      }
      if ($this->helper->shouldLog()) {
        $this->helper->logDeletion($reminder);
      }
      $reminder->delete();
    }
    $form_state->setRebuild();
  }

}
