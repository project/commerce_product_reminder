<?php

namespace Drupal\commerce_product_reminder\Form;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product_reminder\Entity\ReminderInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use PhpParser\Node\Stmt\TryCatch;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_product_reminder\HelperServiceInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Flood\FloodInterface;

/**
 * Class ReminderForm.
 */
class ReminderSubscriptionForm extends FormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\commerce_product_reminder\HelperServiceInterface definition.
   *
   * @var \Drupal\commerce_product_reminder\HelperServiceInterface
   */
  protected $helper;

  /**
   * Drupal\Component\Utility\EmailValidatorInterface definition.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Drupal\Core\Flood\FloodInterface definition.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Drupal\commerce\CommerceContentEntityStorage definition.
   *
   * @var \Drupal\commerce\CommerceContentEntityStorage
   */
  protected $productStorage;

  /**
   * ReminderSubscriptionForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\commerce_product_reminder\HelperServiceInterface $helper
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   * @param \Drupal\Core\Flood\FloodInterface $flood
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, HelperServiceInterface $helper, EmailValidatorInterface $email_validator, FloodInterface $flood, RendererInterface $renderer) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->helper = $helper;
    $this->emailValidator = $email_validator;
    $this->flood = $flood;
    $this->productStorage = $entity_type_manager->getStorage('commerce_product');
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('commerce_product_reminder.helper'),
      $container->get('email.validator'),
      $container->get('flood'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reminder_subscription_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $product_id = NULL) {
    $config = $this->configFactory->get('commerce_product_reminder.settings');
    if (!$product_id) {
      return [];
    }
    $form['product_id'] = [
      '#type' => 'value',
      '#value' => $product_id,
    ];
    $form['intro'] = [
      '#type' => 'processed_text',
      '#text' => $config->get('form.submit_message.value'),
      '#format' => $config->get('form.submit_message.format') ?: filter_fallback_format(),
      '#weight' => 0,
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Leave empty this text field if you are not a bot.'),
      '#maxlength' => 255,
      '#wrapper_attributes' => ['style'=> 'position: absolute; height:0; width:0; overflow:hidden; margin:0; margin-left: -9999px;'],
      '#size' => 64,
      '#weight' => 5,
    ];
    $form['mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#maxlength' => 255,
      '#attributes' => ['class'=> ['js-reminder-mail']],
      '#default_value' => $this->currentUser()->isAuthenticated() ? $this->currentUser()->getEmail() : '',
      '#required' => TRUE,
      '#size' => 64,
      '#weight' => 10,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $config->get('form.submit_label'),
      '#button_type' => 'primary',
    ];

    $form['#attached']['library'][] = 'commerce_product_reminder/reminder_form';
    $form['#attributes']['class'][] = 'js-mail-control';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $product_id = $values['product_id'];
    $product = $this->productStorage->load($product_id);
    if (!$product instanceof ProductInterface) {
      $form_state->setError($form, $this->t('An error occurs. Please retry later.'));
    }

    if (!empty($values['name'])) {
      $form_state->setError($form, $this->t('Hmm...Are you an human !?'));
    }
    if (empty($values['mail'])) {
      $form_state->setError($form['mail'], $this->t('The email is mandatory.'));
    }
    else {
      $reminder = $this->helper->loadReminderByProductAndMail($product, $values['mail']);
      if ($reminder instanceof ReminderInterface) {
        if ($reminder->isEnabled()) {
          $form_state->setError($form['mail'], $this->t('You have already subscribed on this product to receive notifications.'));
        }
        else {
          if ($this->helper->confirmationIsEnabled()) {
            $this->helper->sendConfirmationMail($reminder);
            $form_state->setError($form['mail'], $this->t('You have already subscribed on this product to receive notifications, but you have not confirm yet your subscription. A new confirmation email has been sent. Please confirm your subscription.'));
          }
          else {
            // The visitor has a reminder not confirmed. And confirmation is disabled. So we let him subscribe a new reminder.
          }
        }
      }
    }
    if (!$this->emailValidator->isValid($values['mail'])) {
      $form_state->setError($form['mail'], $this->t('Looks like the email provided is not valid.'));
    }
    // Perform another check on the mail provided. Looks like emailValidator
    // service doesn't handle mail with a space.
    $mail_valid = filter_var($values['mail'], FILTER_VALIDATE_EMAIL);
    if (!$mail_valid) {
      $form_state->setError($form['mail'], $this->t('Looks like the email provided is not valid.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $status_message = $this->getStatusMessage();
    $log = $this->helper->shouldLog();
    $product_id = $values['product_id'];
    $product = $this->productStorage->load($product_id);
    $mail = $values['mail'];
    $confirmation_enabled = $this->helper->confirmationIsEnabled();
    $status = !$confirmation_enabled;

    Try {
      $reminder = $this->entityTypeManager->getStorage('commerce_product_reminder')->create([
        'mail' => $mail,
        'product_id' => $product->id(),
        'status' => $status,
      ]);
      $reminder->save();
      $this->messenger()->addStatus($status_message);
      if ($log) {
        $this->logger('commerce_product_reminder')->info($this->t('A reminder has been created for product @product and mail @mail', ['@product' => $product->toLink($product->label())->toString(), '@mail' =>$mail]));
      }
    }
    catch (\Exception $e) {
      $this->messenger()->addError($this->t('An error occurs. Please retry later.'));
      $this->logger('commerce_product_reminder')->error($this->t('An error occurs when trying to save a reminder for product @product and mail @mail with the error message:<br />@message', ['@product' => $product->toLink($product->label())->toString(), '@mail' =>$mail, '@message' => $e->getMessage()]));
    }
  }

  /**
   * Get the status message.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The status message.
   */
  protected function getStatusMessage() {
    $config = $this->configFactory->get('commerce_product_reminder.settings');
    $status_message = [
      '#type' => 'processed_text',
      '#text' => $config->get('form.status_message.value'),
      '#format' => $config->get('form.status_message.format') ?: filter_fallback_format(),
    ];
    $status_message = $this->renderer->renderRoot($status_message);
    return $status_message;
  }

}
