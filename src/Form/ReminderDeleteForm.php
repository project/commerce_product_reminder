<?php

namespace Drupal\commerce_product_reminder\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Reminder entities.
 *
 * @ingroup commerce_product_reminder
 */
class ReminderDeleteForm extends ContentEntityDeleteForm {


}
