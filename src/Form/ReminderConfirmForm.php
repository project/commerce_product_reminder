<?php

namespace Drupal\commerce_product_reminder\Form;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product_reminder\Entity\ReminderInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_product_reminder\HelperServiceInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Flood\FloodInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ReminderConfirmForm.
 */
class ReminderConfirmForm extends FormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\commerce_product_reminder\HelperServiceInterface definition.
   *
   * @var \Drupal\commerce_product_reminder\HelperServiceInterface
   */
  protected $helper;

  /**
   * Drupal\Component\Utility\EmailValidatorInterface definition.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Drupal\commerce\CommerceContentEntityStorage definition.
   *
   * @var \Drupal\commerce\CommerceContentEntityStorage
   */
  protected $productStorage;

  /**
   * Drupal\commerce_product_reminder\ReminderStorageInterface definition.
   *
   * @var \Drupal\commerce_product_reminder\ReminderStorageInterface
   */
  protected $reminderStorage;

  /**
   * ReminderSubscriptionForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\commerce_product_reminder\HelperServiceInterface $helper
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, HelperServiceInterface $helper, EmailValidatorInterface $email_validator, RendererInterface $renderer) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->helper = $helper;
    $this->emailValidator = $email_validator;
    $this->renderer = $renderer;
    $this->productStorage = $entity_type_manager->getStorage('commerce_product');
    $this->reminderStorage = $entity_type_manager->getStorage('commerce_product_reminder');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('commerce_product_reminder.helper'),
      $container->get('email.validator'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reminder_manage_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $hash = NULL) {
    $config = $this->configFactory->get('commerce_product_reminder.settings');
    if (!$this->helper->confirmationIsEnabled()) {
      throw new NotFoundHttpException();
    }
    $mail = $form_state->get('mail_to_confirm');
    $product = $form_state->get('product');
    $reminder = $this->helper->loadReminderByHash($hash, FALSE);
    if (!$reminder instanceof ReminderInterface) {
      throw new NotFoundHttpException();
    }

    $form_state->set('reminder', $reminder);
    if (empty($mail)) {
      $mail = $reminder->getMail();
      $form_state->set('mail_to_confirm', $mail);
    }
    if (empty($product)) {
      $product = $reminder->getProduct();
      $form_state->set('product', $product);
    }
    $mail_masked = $this->helper->maskEmail($mail);

    if ($reminder->isEnabled()) {
      $form['intro'] = [
        '#type' => 'markup',
        '#markup' => $this->t('Your reminder subscription for @mail_masked on @label is confirmed.', [
          '@mail_masked' => $mail_masked,
          '@label' => $product->label(),
        ]),
        '#wrapper_attributes' => ['class' => ['confirm-reminders__message']],
        '#weight' => 0,
      ];
    }
    else {
      $form['intro'] = [
        '#type' => 'markup',
        '#markup' => $this->t('Confirm your reminder subscription for @mail_masked on @label', [
          '@mail_masked' => $mail_masked,
          '@label' => $product->label(),
        ]),
        '#wrapper_attributes' => ['class' => ['confirm-reminders__message']],
        '#weight' => 0,
      ];

      $form['mail'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Confirm Email'),
        '#description' => $this->t('Please confirm your mail to enable your subscription'),
        '#maxlength' => 255,
        '#attributes' => ['class'=> ['js-reminder-mail']],
        '#default_value' => '',
        '#required' => TRUE,
        '#size' => 64,
        '#weight' => 10,
      ];
      $form['actions']['#type'] = 'actions';
      $form['actions']['#weight'] = 100;
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Confirm'),
        '#button_type' => 'primary',
      ];
    }

    $form['#attached']['library'][] = 'commerce_product_reminder/reminder_form';
    $form['#attributes']['class'][] = 'js-mail-control';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (empty($values['mail'])) {
      $form_state->setError($form['mail'], $this->t('The confirm email is mandatory.'));
    }
    if (!$this->emailValidator->isValid($values['mail'])) {
      $form_state->setError($form['mail'], $this->t('Looks like the confirm email provided is not valid.'));
    }

    $mail_confirmed = $values['mail'];
    $mail_to_confirm = $form_state->get('mail_to_confirm');
    if ($mail_confirmed !== $mail_to_confirm) {
      $form_state->setError($form['mail'], $this->t("The confirm email doesn't match the email for which you have reminders notifications."));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $reminder = $form_state->get('reminder');
    if ($reminder instanceof ReminderInterface) {
      $reminder->setStatus(TRUE);
      $reminder->save();
    }
    $form_state->setRebuild();
  }

}
