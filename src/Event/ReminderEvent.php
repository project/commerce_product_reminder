<?php

namespace Drupal\commerce_product_reminder\Event;

use Drupal\commerce_product_reminder\Entity\ReminderInterface;

/**
 * Defines the reminder event.
 *
 * @see \Drupal\commerce_product_reminder\Event\ReminderEvents
 */
class ReminderEvent extends EventBase {

  /**
   * The reminder.
   *
   * @var \Drupal\commerce_product_reminder\Entity\ReminderInterface
   */
  protected $reminder;

  /**
   * Constructs a new ReminderEvent.
   *
   * @param \Drupal\commerce_product_reminder\Entity\ReminderInterface $reminder
   *   The reminder.
   */
  public function __construct(ReminderInterface $reminder) {
    $this->reminder = $reminder;
  }

  /**
   * Gets the reminder.
   *
   * @return \Drupal\commerce_product_reminder\Entity\ReminderInterface
   *   Gets the reminder.
   */
  public function getReminder(): ReminderInterface {
    return $this->reminder;
  }

}
