<?php

namespace Drupal\commerce_product_reminder\Event;

final class ReminderEvents {

  /**
   * Name of the event fired after loading a reminder.
   *
   * @Event
   *
   * @see \Drupal\commerce_product_reminder\Event\ReminderEvent
   */
  const REMINDER_LOAD = 'commerce_product_reminder.load';

  /**
   * Name of the event fired after creating a new reminder.
   *
   * Fired before the reminder is saved.
   *
   * @Event
   *
   * @see \Drupal\commerce_product_reminder\Event\ReminderEvent
   */
  const REMINDER_CREATE = 'commerce_product_reminder.create';

  /**
   * Name of the event fired before saving an reminder.
   *
   * @Event
   *
   * @see \Drupal\commerce_product_reminder\Event\ReminderEvent
   */
  const REMINDER_PRESAVE = 'commerce_product_reminder.presave';

  /**
   * Name of the event fired after saving a new reminder.
   *
   * @Event
   *
   * @see \Drupal\commerce_product_reminder\Event\ReminderEvent
   */
  const REMINDER_INSERT = 'commerce_product_reminder.insert';

  /**
   * Name of the event fired after saving an existing reminder.
   *
   * @Event
   *
   * @see \Drupal\commerce_product_reminder\Event\ReminderEvent
   */
  const REMINDER_UPDATE = 'commerce_product_reminder.update';

  /**
   * Name of the event fired before deleting a reminder.
   *
   * @Event
   *
   * @see \Drupal\commerce_product_reminder\Event\ReminderEvent
   */
  const REMINDER_PREDELETE = 'commerce_product_reminder.predelete';

  /**
   * Name of the event fired after deleting an order.
   *
   * @Event
   *
   * @see \Drupal\commerce_product_reminder\Event\ReminderEvent
   */
  const REMINDER_DELETE = 'commerce_product_reminder.delete';

}
