<?php

namespace Drupal\commerce_product_reminder\Plugin\QueueWorker;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product_reminder\Entity\ReminderInterface;
use Drupal\commerce_product_reminder\HelperServiceInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Send reminder email on CRON run.
 *
 * @QueueWorker(
 *   id = "commerce_product_reminder_worker",
 *   title = @Translation("Reminder worker"),
 *   cron = {"time" = 30}
 * )
 */
class ReminderWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The helper service.
   *
   * @var \Drupal\commerce_product_reminder\HelperServiceInterface
   */
  protected $helper;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct the worker.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_product_reminder\HelperServiceInterface $helper
   *   The helper service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, HelperServiceInterface $helper, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->helper = $helper;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_product_reminder.helper'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!$this->helper->isEnabled()) {
      return;
    }
    $product_variation_id = $data['product_variation_id'];
    $reminder_id = $data['reminder_id'];
    $product_variation = $this->entityTypeManager->getStorage('commerce_product_variation')->load($product_variation_id);
    $reminder = $this->entityTypeManager->getStorage('commerce_product_reminder')->load($reminder_id);
    if ($product_variation instanceof ProductVariationInterface && $reminder instanceof ReminderInterface) {
      if (!$reminder->isEnabled()) {
        return;
      }
      $this->helper->sendMail($product_variation, $reminder);
    }
  }

}
