<?php

namespace Drupal\commerce_product_reminder;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product_reminder\Entity\ReminderInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Class HelperService.
 */
class HelperService implements HelperServiceInterface {

  use StringTranslationTrait;

  /**
   * The logger channel factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Entity\EntityTypeBundleInfoInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Drupal\Component\Utility\EmailValidatorInterface definition.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The mail handler service.
   *
   * @var \Drupal\commerce_product_reminder\MailHandlerInterface
   */
  protected $mailHandler;

  /**
   * Drupal\commerce_product_reminder\ReminderStorageInterface definition.
   *
   * @var \Drupal\commerce_product_reminder\ReminderStorageInterface
   */
  protected $reminderStorage;

  /**
   * HelperService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   * @param \Drupal\Core\Utility\Token $token
   * @param \Drupal\commerce_product_reminder\MailHandlerInterface $mail_handler
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, AccountProxyInterface $current_user, EntityTypeBundleInfoInterface $entity_type_bundle_info, EmailValidatorInterface $email_validator, Token $token, MailHandlerInterface $mail_handler, LoggerChannelFactoryInterface $logger_factory) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
    $this->currentUser = $current_user;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->emailValidator = $email_validator;
    $this->token = $token;
    $this->mailHandler = $mail_handler;
    $this->loggerFactory = $logger_factory;
    $this->reminderStorage = $entity_type_manager->getStorage('commerce_product_reminder');
  }

  /**
   * Gets the settings config object.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The configuration.
   */
  protected function getConfig(): ImmutableConfig {
    return $this->configFactory->get('commerce_product_reminder.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeBundles($with_label = TRUE, $entity_type = 'commerce_product') {
    $bundles = [];
    $entity_type_bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type);

    foreach ($entity_type_bundles as $name => $value) {
      $bundles[$name] = $with_label ? $entity_type_bundles[$name]['label'] : $name;
    }
    return $bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrivateKey() {
    $config = $this->configFactory->getEditable('commerce_product_reminder.settings');
    $key = $config->get('private_key');
    if (!$key) {
      //  This will create a 32 character identifier (a 128 bit hex number) that is extremely difficult to predict
      $key = md5(uniqid(rand()));
      $config->set('private_key', $key)->save(TRUE);
    }
    return $key;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrivateIv() {
    $config = $this->configFactory->getEditable('commerce_product_reminder.settings');
    $iv = $config->get('private_iv');
    if (!$iv) {
      //  This will create a 16 character identifier (a 128 bit hex number) that is extremely difficult to predict
      $iv = mb_substr(md5(uniqid(rand())), 0, 16);
      $config->set('private_iv', $iv)->save(TRUE);
    }
    return $iv;
  }

  /**
   * {@inheritdoc}
   */
  public function loadReminderByProductAndMail(ProductInterface $product, $mail, $active_only = TRUE) {
    $reminders = $this->reminderStorage->loadMultipleByProductAndMail($product, $mail, $active_only);
    return !empty($reminders) ? reset($reminders) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadRemindersByProduct(ProductInterface $product): array {
    $reminders = $this->reminderStorage->loadMultipleByProduct($product, FALSE);
    return $reminders;
  }

  /**
   * {@inheritdoc}
   */
  public function loadReminderByHash($hash, bool $active_only = TRUE) {
    $reminder = NULL;
    // Attempt to detect invalid format.
    if (!preg_match('/.{20}t[0-9]+c./', $hash)) {
      return $reminder;
    }

    $md5 = mb_substr($hash, 0, 20);
    [$product_id, $mail_encrypted] = explode('c', mb_substr($hash, 21), 2);
    $mail_decrypted = $this->decrypt($mail_encrypted);
    if (!$this->emailValidator->isValid($mail_decrypted)) {
      return $reminder;
    }

    // Check the hash if the comparison fails, return a not found error.
    if ($md5 !== mb_substr(md5($mail_decrypted . $this->getPrivateKey()), 0, 20)) {
      return $reminder;
    }

    $product = $this->entityTypeManager->getStorage('commerce_product')->load($product_id);
    if ($product instanceof ProductInterface) {
      $reminder = $this->loadReminderByProductAndMail($product, $mail_decrypted, $active_only);
    }
    return $reminder;
  }

  /**
   * Base64 encode a string.
   *
   * @param $data
   *
   * @return string
   */
  public function base64urlEncode($data): string {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
  }

  /**
   * Base64 decode a string.
   *
   * @param $data
   *
   * @return bool|string
   */
  public function base64urlDecode($data) {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
  }

  /**
   * Encrypt a string.
   *
   * @param $string
   *   The string to encrypt.
   *
   * @return string
   *   The string encrypted.
   */
  public function encrypt($string): string {
    $key = $this->getPrivateKey();
    $iv = $this->getPrivateIv();
    $method = 'AES-256-CTR';
    $string_encrypted = $this->base64urlEncode(openssl_encrypt($string, $method , $key,0, $iv));
    return $string_encrypted;
  }

  /**
   * Decrypt a string.
   *
   * @param $string
   *   The string encrypted.
   *
   * @return string
   *   The string decrypted.
   */
  public function decrypt($string): string {
    $key = $this->getPrivateKey();
    $iv = $this->getPrivateIv();
    $method = 'AES-256-CTR';
    $string_decrypted = openssl_decrypt($this->base64urlDecode($string), $method , $key, 0, $iv);
    return $string_decrypted;
  }

  /**
   * Generate the hash key used for the unsubscribe link.
   *
   * @param $mail
   * @param $product_id
   *
   * @return string
   */
  public function generateHash($mail, $product_id): string {
    $mail_encrypted = $this->encrypt($mail);
    return mb_substr(md5($mail . $this->getPrivateKey()), 0, 20) . 't' . $product_id . 'c' . $mail_encrypted;
  }

  /**
   * {@inheritdoc}
   */
  public function replaceToken($string, ProductInterface $product = NULL, ProductVariationInterface $product_variation = NULL): string {
    $data = [];
    if ($product_variation) {
      $data['commerce_product_variation'] = $product_variation;
    }
    if ($product) {
      $data['commerce_product'] = $product;
    }
    $string = $this->token->replace($string, $data, ['clear' => TRUE]);
    return $string;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemindersFromVariation(ProductVariationInterface $product_variation): array {
    $reminders = [];
    $product = $product_variation->getProduct();
    if ($product instanceof ProductInterface) {
      $reminders = $this->reminderStorage->loadMultipleByProduct($product);
    }
    return $reminders;
  }

  /**
   * {@inheritdoc}
   */
  public function useCron(): bool {
    return (bool) $this->getConfig()->get('general.use_cron');
  }

  /**
   * {@inheritdoc}
   */
  public function shouldLog(): bool {
    return (bool) $this->getConfig()->get('general.log');
  }

  /**
   * {@inheritdoc}
   */
  public function logDeletion(ReminderInterface $reminder) {
    $mail = $reminder->getMail();
    $product_id = $reminder->getProductId();
    $username = $this->currentUser->getDisplayName();
    $this->loggerFactory->get('commerce_product_reminder')->info($this->t('The reminder for mail @mail and product id @product_id has been deleted by @username', [
      '@mail' =>$mail,
      '@product_id' => $product_id,
      '@username' =>$username,
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function logDeletionError(ReminderInterface $reminder) {
    $mail = $reminder->getMail();
    $product_id = $reminder->getProductId();
    $username = $this->currentUser->getDisplayName();
    $this->loggerFactory->get('commerce_product_reminder')->error($this->t('An error occurs when deleting the reminder for mail @mail and product id @product_id with user @username', [
      '@mail' =>$mail,
      '@product_id' => $product_id,
      '@username' =>$username,
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->getConfig()->get('general.enabled');
  }

  /**
   * {@inheritdoc}
   */
  public function confirmationIsEnabled(): bool {
    return (bool) $this->getConfig()->get('confirmation.enabled');
  }

  /**
   * {@inheritdoc}
   */
  public function getSubject(): string {
    return $this->getConfig()->get('mail.subject');
  }

  /**
   * {@inheritdoc}
   */
  public function getBody(): array {
    return $this->getConfig()->get('mail.body');
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailFrom(): string {
    $mail_from = $this->getConfig()->get('mail.from');
    return !empty($mail_from) ? $mail_from : '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmationSubject(): string {
    return $this->getConfig()->get('confirmation.mail.subject');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmationBody(): array {
    return $this->getConfig()->get('confirmation.mail.body');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmationEmailFrom(): string {
    $mail_from = $this->getConfig()->get('confirmation.mail.from');
    return !empty($mail_from) ? $mail_from : '';
  }

  /**
   * {@inheritdoc}
   */
  function maskEmail($mail): string {
    if (preg_match('/^(.).*@(.).*(\..+)$/', $mail)) {
      return preg_replace('/^(.).*@(.).*(\..+)$/', '$1*****@$2*****$3', $mail);
    }
    else {
      // Missing top-level domain.
      return preg_replace('/^(.).*@(.).*$/', '$1*****@$2*****', $mail);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendMail(ProductVariationInterface $product_variation, ReminderInterface $reminder): bool {
    if (!$this->isEnabled()) {
      return FALSE;
    }
    $product = $reminder->getProduct();
    if (!$product) {
      return FALSE;
    }
    $subject = $this->getSubject();
    $subject = $this->replaceToken($subject, $product, $product_variation);
    $body = $this->getBody();
    $body['value'] = $this->replaceToken($body['value'], $product, $product_variation);

    $body_array = [
      '#type' => 'processed_text',
      '#text' => $body['value'],
      '#format' =>$body['format'],
    ];
    $hash = $this->generateHash($reminder->getMail(), $product->id());
    $manage_url = Url::fromRoute('commerce_product_reminder.manage_reminder', ['hash' => $hash], ['absolute' => TRUE]);
    $body_mail = [
      '#theme' => 'commerce_product_reminder_mail',
      '#product_variation' => $product_variation,
      '#body' => $body_array,
      '#manage_url' => $manage_url->toString(),
    ];
    $params = ['from' => $this->getEmailFrom()];
    return $this->mailHandler->sendMail($reminder->getMail(), $subject, $body_mail, $params);
  }

  /**
   * {@inheritdoc}
   */
  public function sendConfirmationMail(ReminderInterface $reminder): bool {
    if (!$this->isEnabled() || !$this->confirmationIsEnabled()) {
      return FALSE;
    }
    $product = $reminder->getProduct();
    if (!$product instanceof ProductInterface) {
      return FALSE;
    }
    $subject = $this->getConfirmationSubject();
    $subject = $this->replaceToken($subject, $product);
    $body = $this->getConfirmationBody();
    $body['value'] = $this->replaceToken($body['value'], $product);

    $body_array = [
      '#type' => 'processed_text',
      '#text' => $body['value'],
      '#format' =>$body['format'],
    ];
    $hash = $this->generateHash($reminder->getMail(), $product->id());
    $confirm_url = Url::fromRoute('commerce_product_reminder.confirm_reminder', ['hash' => $hash], ['absolute' => TRUE]);
    $body_mail = [
      '#theme' => 'commerce_product_reminder_confirm_mail',
      '#product' => $product,
      '#body' => $body_array,
      '#confirm_url' => $confirm_url->toString(),
    ];
    $params = ['from' => $this->getConfirmationEmailFrom()];
    return $this->mailHandler->sendMail($reminder->getMail(), $subject, $body_mail, $params);
  }

}
