<?php

namespace Drupal\commerce_product_reminder\EventSubscriber;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_product\Event\ProductEvents;
use Drupal\commerce_product\Event\ProductVariationEvent;
use Drupal\Core\Queue\QueueFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_product_reminder\HelperServiceInterface;

/**
 * Class ProductVariationSubscriber.
 */
class ProductVariationSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\commerce_product_reminder\HelperServiceInterface definition.
   *
   * @var \Drupal\commerce_product_reminder\HelperServiceInterface
   */
  protected $helper;

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * ProductVariationSubscriber constructor.
   *
   * @param \Drupal\commerce_product_reminder\HelperServiceInterface $helper
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   */
  public function __construct(HelperServiceInterface $helper, QueueFactory $queue_factory) {
    $this->helper = $helper;
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ProductEvents::PRODUCT_VARIATION_INSERT] = ['onProductVariationInsert', -10000];
    $events[ProductEvents::PRODUCT_VARIATION_UPDATE] = ['onProductVariationUpdate', -10000];
    return $events;
  }

  /**
   * This method is called when the product_variation_insert event is dispatched.
   *
   * @param \Drupal\commerce_product\Event\ProductVariationEvent $event
   *   The dispatched event.
   */
  public function onProductVariationInsert(ProductVariationEvent $event) {
    $product_variation = $event->getProductVariation();
    if ($product_variation->isPublished()) {
      $this->sendMailForReminderRelated($product_variation);
    }
  }

  /**
   * This method is called when the product_variation_update event is dispatched.
   *
   * @param \Drupal\commerce_product\Event\ProductVariationEvent $event
   *   The dispatched event.
   */
  public function onProductVariationUpdate(ProductVariationEvent $event) {
    $product_variation = $event->getProductVariation();
    $product_variation_original = $product_variation->original;
    if (!$product_variation_original instanceof ProductVariationInterface) {
      return;
    }
    if ($product_variation->isPublished() && !$product_variation_original->isPublished()) {
      $this->sendMailForReminderRelated($product_variation);
    }
  }

  /**
   * Send reminder mails or queue them.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation
   */
  protected function sendMailForReminderRelated(ProductVariationInterface $product_variation) {
    if (!$this->helper->isEnabled()) {
      return;
    }
    $data = [];
    $data['product_variation_id'] = $product_variation->id();
    $reminders = $this->helper->getRemindersFromVariation($product_variation);
    foreach ($reminders as $reminder) {
      $data['reminder_id'] = $reminder->id();
      if ($this->helper->useCron()) {
        $this->queueFactory->get('commerce_product_reminder_worker')->createItem($data);
      }
      else {
        $this->helper->sendMail($product_variation, $reminder);
      }
    }
  }


}
