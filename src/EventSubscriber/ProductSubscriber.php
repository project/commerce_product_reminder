<?php

namespace Drupal\commerce_product_reminder\EventSubscriber;

use Drupal\commerce_product\Event\ProductEvent;
use Drupal\commerce_product\Event\ProductEvents;
use Drupal\Core\Queue\QueueFactory;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\commerce_product_reminder\HelperServiceInterface;

/**
 * Class ProductSubscriber.
 */
class ProductSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\commerce_product_reminder\HelperServiceInterface definition.
   *
   * @var \Drupal\commerce_product_reminder\HelperServiceInterface
   */
  protected $helper;

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * ProductVariationSubscriber constructor.
   *
   * @param \Drupal\commerce_product_reminder\HelperServiceInterface $helper
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   */
  public function __construct(HelperServiceInterface $helper, QueueFactory $queue_factory) {
    $this->helper = $helper;
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ProductEvents::PRODUCT_DELETE] = ['onProductDelete'];
    return $events;
  }

  /**
   * This method is called when the product_variation_insert event is dispatched.
   *
   * @param \Drupal\commerce_product\Event\ProductEvent $event
   *   The dispatched event.
   */
  public function onProductDelete(ProductEvent $event) {
    $product = $event->getProduct();
    $reminders = $this->helper->loadRemindersByProduct($product);
    foreach ($reminders as $reminder) {
      if ($this->helper->useCron()) {
        $data = [];
        $data[$reminder->id()] = $reminder->id();
        $this->queueFactory->get('commerce_product_reminder_deletion_worker')->createItem($data);
      }
      else {
        Try {
          if ($this->helper->shouldLog()) {
            $this->helper->logDeletion($reminder);
          }
          $reminder->delete();
        }
        catch (Exception $e) {
          $this->helper->logDeletionError($reminder);
        }
      }
    }
  }

}
