<?php

namespace Drupal\commerce_product_reminder\EventSubscriber;

use Drupal\commerce_product_reminder\Event\ReminderEvent;
use Drupal\commerce_product_reminder\Event\ReminderEvents;
use Drupal\commerce_product_reminder\HelperServiceInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sends a confirmation email when a reminder is inserted.
 */
class ReminderInsertSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The helper service.
   *
   * @var \Drupal\commerce_product_reminder\HelperServiceInterface
   */
  protected $helper;

  /**
   * Constructs a new OrderReceiptSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_product_reminder\HelperServiceInterface $helper
   *   The helper service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, HelperServiceInterface $helper) {
    $this->entityTypeManager = $entity_type_manager;
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [ReminderEvents::REMINDER_INSERT => ['sendConfirmEmail', -100]];
    return $events;
  }

  /**
   * Sends an confirmation email.
   *
   * @param \Drupal\commerce_product_reminder\Event\ReminderEvent $event
   *   The event we subscribed to.
   */
  public function sendConfirmEmail(ReminderEvent $event) {
    if (!$this->helper->isEnabled()) {
      return;
    }
    if (!$this->helper->confirmationIsEnabled()) {
      return;
    }
    /** @var \Drupal\commerce_product_reminder\Entity\ReminderInterface $order */
    $reminder = $event->getReminder();
    if ($reminder->isEnabled()) {
      return;
    }
    $this->helper->sendConfirmationMail($reminder);
  }


}
