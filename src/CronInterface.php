<?php

namespace Drupal\commerce_product_reminder;

/**
 * Provides the interface for the reminder cron.
 */
interface CronInterface {

  /**
   * Runs the cron.
   */
  public function run();

}
