<?php

namespace Drupal\commerce_product_reminder;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Reminder entities.
 *
 * @ingroup commerce_product_reminder
 */
class ReminderListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new NodeListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, RedirectDestinationInterface $redirect_destination) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Reminder ID');
    $header['mail'] = $this->t('Mail');
    $header['status'] = $this->t('Status');
    $header['user'] = $this->t('User');
    $header['product'] = $this->t('Product');
    $header['created'] = $this->t('Created');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\commerce_product_reminder\Entity\Reminder */
    $row['id'] = $entity->id();
    $row['mail'] = Link::createFromRoute(
      $entity->label(),
      'entity.commerce_product_reminder.edit_form',
      ['commerce_product_reminder' => $entity->id()]
    );
    $row['status'] = $entity->isEnabled() ? $this->t('Enabled') : $this->t('Disabled');
    $row['user'] = $this->getUserName($entity);
    $row['product'] = $this->getProductName($entity);
    $row['created'] = $this->dateFormatter->format($entity->getChangedTime(), 'short');
    return $row + parent::buildRow($entity);
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return \Drupal\Core\Link|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected function getUserName(EntityInterface $entity) {
    /* @var $entity \Drupal\commerce_product_reminder\Entity\Reminder */
    $user = $entity->getUser();
    if ($user instanceof UserInterface) {
      $username = Link::createFromRoute(
        $user->getDisplayName(),
        'entity.user.canonical',
        ['user' => $user->id()]
      );
    }
    else {
      $username = $this->t('Anonymous');
    }
    return $username;
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return \Drupal\Core\Link|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected function getProductName(EntityInterface $entity) {
    /* @var $entity \Drupal\commerce_product_reminder\Entity\Reminder */
    $product = $entity->getProduct();
    if ($product instanceof ProductInterface) {
      $product_name = Link::createFromRoute(
        $product->label(),
        'entity.commerce_product.canonical',
        ['commerce_product' => $product->id()]
      );
    }
    else {
      $product_name = $this->t('Unknown');
    }
    return $product_name;
  }

}
