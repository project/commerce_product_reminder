<?php

namespace Drupal\commerce_product_reminder;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Reminder entity.
 *
 * @see \Drupal\commerce_product_reminder\Entity\Reminder.
 */
class ReminderAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\commerce_product_reminder\Entity\ReminderInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isEnabled()) {
          return AccessResult::allowedIfHasPermission($account, 'view disabled reminder entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view enabled reminder entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit reminder entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete reminder entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add reminder entities');
  }

}
