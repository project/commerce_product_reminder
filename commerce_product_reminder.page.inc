<?php

/**
 * @file
 * Contains reminder.page.inc.
 *
 * Page callback for Reminder entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Reminder templates.
 *
 * Default template: commerce-product-reminder.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_commerce_product_reminder(array &$variables) {
  // Fetch Reminder Entity Object.
  $reminder = $variables['elements']['#commerce_product_reminder'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
